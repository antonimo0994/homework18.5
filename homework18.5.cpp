﻿#include <iostream>
#include <algorithm> 
#include <cstdlib>

using namespace std;

class Player
{
    string name;
    int points;

public:
		friend bool point_sorter (const Player&, const Player&);

		string get_name()
		{
			return name;
		}

		int get_points()
		{
			return points;
		}

		void set_name(string n)
		{
			name = n; 
		}

		void set_points(int n) 
		{
			points = n; 
		}

	};

	bool point_sorter(const Player& l, const Player& r)
	{
		return l.points > r.points;
	}

	int main() {
		system("chcp 1251 > nul");
		size_t n;
		cout << "Введите число игроков ";
		cin >> n;
		Player* players = new Player[n];
		for (size_t i = 0; i < n; i++)
		{
			string name;
			int point;
			cout << "Введите данные игрока " << i << " (имя и очки): ";
			cin >> name >> point;
			players[i].set_name(name);
			players[i].set_points(point);
		}
		sort(players, players + n, point_sorter);
		for (size_t i = 0; i < n; i++)
		{
			cout << players[i].get_name() << " " << players[i].get_points() << '\n';
		}
		delete[] players;
	}